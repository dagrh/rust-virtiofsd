// Copyright (C) 2019 Red Hat, Inc. and/or its affiliates

use libc;
use std::fs::File;
use std::os::unix::io::{RawFd, FromRawFd};
use vhostuser_rs::*;
use vm_memory::*;

pub const MAX_VRING_NUM: usize = 2;

#[derive(Debug, Default)]
pub struct VirtioFSSlave {
    pub call_fd: [Option<RawFd>; MAX_VRING_NUM],
    mmap: Option<GuestMemoryMmap>
}

impl VirtioFSSlave {
    pub fn new() -> Self {
        VirtioFSSlave {
            ..Default::default()
        }
    }
}

// TODO: Needs to go somewhere central
const VIRTIO_F_VERSION_1:u64 = 1<<32;

impl VhostUserSlave for VirtioFSSlave {
    // The master wants to know what features we support
    fn get_features(&mut self) -> Result<u64> {
        Ok(VIRTIO_F_VERSION_1)
    }
    fn get_protocol_features(&mut self) -> Result<vhostuser_rs::message::VhostUserProtocolFeatures> {
        Ok(message::VhostUserProtocolFeatures::empty())
    }

    // 'This can be used on the Slave as a "session start"'
    fn set_owner(&mut self) -> Result<()> {
        Ok(())
    }

    // Gives an fd for the slave to kick the master for a given queue
    fn set_vring_call(&mut self, index: u8, fd: Option<RawFd>) -> Result<()> {
        if index as usize >= MAX_VRING_NUM {
            return Err(Error::InvalidParam);
        }
        if self.call_fd[index as usize].is_some() {
            // Close file descriptor set by previous operations.
            let _ = nix::unistd::close(self.call_fd[index as usize].unwrap());
        }
        self.call_fd[index as usize] = fd;
        Ok(())
    }

    // Host tells us which features it can handle
    fn set_features(&mut self, _features: u64) -> Result<()> {
        // Check if it's got features we need
        dbg!("TODO");
        Ok(())
    }

    // We get given an fd, an offset in the file and an offset in GPA corresponding to
    // a memory region (we're also given HVA in qemu, but we don't need that)
    // TODO: These can come I think when we're using the queues, we have
    // to be careful of safety when mapping it
    fn set_mem_table(&mut self, ctx: &[message::VhostUserMemoryRegion], fds: &[RawFd]) -> Result<()> {
        assert_eq!(ctx.len(), fds.len());
        let mut res = Ok(());
        let mut mappings = Vec::new();
        // Note: Even if we fail part way we complete the loop to consume all the fds
        for (msg, fd) in ctx.iter().zip(fds.iter()) {
            let file = unsafe { File::from_raw_fd(*fd) };
            if res.is_ok() {
                dbg!(msg);
                // TODO: Stash msg.userspace_addr somewhere
                // safe because MmapRegion is stripping this straight back to the RawFd again
                let mmap_reg = MmapRegion::from_fd(&file, msg.memory_size as usize, msg.mmap_offset as libc::off_t);
                if mmap_reg.is_ok() {
                    let guest_reg = GuestRegionMmap::new(mmap_reg.unwrap(), GuestAddress(msg.guest_phys_addr));
                    mappings.push(guest_reg);
                } else {
                    res = Err(Error::OperationFailedInSlave);
                }
            }
        }

        if res.is_err() { return res; }

        let guestmm = match GuestMemoryMmap::from_regions(mappings) {
            Err(_e) => return Err(Error::OperationFailedInSlave),
            Ok(mm) => mm,
        };

        dbg!(&guestmm);
        self.mmap = Some(guestmm);

        Ok(())
    }

    // Declares the size of queue (index) to be (num)
    fn set_vring_num(&mut self, index: u32, num: u32) -> Result<()> {
        dbg!(index);
        dbg!(num);
        dbg!("set_vring_num TODO");
        Ok(())
    }

    // TODO: Need to understand what this is
    fn set_vring_base(&mut self, index: u32, base: u32) -> Result<()> {
        dbg!(index);
        dbg!(base);
        dbg!("set_vring_base TODO");
        Ok(())
    }

    fn reset_owner(&mut self) -> Result<()> { panic!("TODO") }
    fn set_protocol_features(&mut self, _features: u64) -> Result<()> { panic!("TODO") }

    fn get_queue_num(&mut self) -> Result<u64> { panic!("TODO") }

    fn set_vring_addr(
        &mut self,
        _index: u32,
        _flags: message::VhostUserVringAddrFlags,
        _descriptor: u64,
        _used: u64,
        _available: u64,
        _log: u64,
    ) -> Result<()> { panic!("TODO") }
    fn get_vring_base(&mut self, _index: u32) -> Result<message::VhostUserVringState> { panic!("TODO") }
    fn set_vring_kick(&mut self, _index: u8, _fd: Option<RawFd>) -> Result<()> { panic!("TODO") }
    fn set_vring_err(&mut self, _index: u8, _fd: Option<RawFd>) -> Result<()> { panic!("TODO") }
    fn set_vring_enable(&mut self, _index: u32, _enable: bool) -> Result<()> { panic!("TODO") }

    fn get_config(
        &mut self,
        _offset: u32,
        _size: u32,
        _flags: message::VhostUserConfigFlags,
    ) -> Result<Vec<u8>> { panic!("TODO") }
    fn set_config(&mut self, _offset: u32, _buf: &[u8], _flags: message::VhostUserConfigFlags) -> Result<()> { panic!("TODO") }

}

