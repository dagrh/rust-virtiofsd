// Copyright (C) 2019 Red Hat, Inc. and/or its affiliates

use std::env;
use std::sync::{Arc, Mutex};
use vhostuser_rs::{Error, Listener, Slave};

mod virtiofs_slave;
use virtiofs_slave::VirtioFSSlave;

fn main() ->Result<(), Error> {
    if env::args().len() != 2 {
        eprintln!("Error: Expecting socket path");
    } else {
        let listener = Listener::new(&env::args().nth(1).unwrap()).expect("Listening on UNIX socket");
        // We want to wait for the connection
        listener.set_nonblocking(false)?;
        let endpoint = listener.accept().expect("Accepting socket connection").expect("endpoint");
        dbg!("We have an endpoint");

        let mut slave = Slave::new(endpoint,
                                   Arc::new(Mutex::new(VirtioFSSlave::new())));

        loop {
            slave.handle_request()?
        }
    }

    Ok(())
}
